class BasePage:
    def __init__(self, driver):
        self.driver = driver

    def clear_and_send_keys(self, locator, text):
        element = self.driver.find_element(*locator)
        element.clear()
        element.send_keys(text)
