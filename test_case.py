from locators import LocatorsConstants
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from base_page import BasePage
from home_page import HomePage
from login_page import LoginPage

class TestLogin:
    def setup_method(self):
        options = Options()
        options.page_load_strategy = 'normal'
        self.driver = webdriver.Chrome(options=options)
        self.driver.get("https://admin-demo.nopcommerce.com/")
        self.login_page = LoginPage(self.driver)
        self.home_page = HomePage(self.driver)

    def test_login(self):
        self.login_page.login('admin@yourstore.com', 'admin')
        assert self.home_page.is_displayed()

    def teardown_method(self):
        self.driver.quit()

TestLogin()
