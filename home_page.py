from selenium.webdriver.common.by import By
from base_page import BasePage
from locators import LocatorsConstants

class HomePage(BasePage):
    USER_NAME_LOCATOR = (By.XPATH, LocatorsConstants.USER_NAME_XPATH)

    def is_displayed(self):
        return self.driver.find_element(*self.USER_NAME_LOCATOR).is_displayed()

