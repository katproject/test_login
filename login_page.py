from selenium.webdriver.common.by import By
from base_page import BasePage
from locators import LocatorsConstants

class LoginPage(BasePage):
    USER_LOGIN_LOCATOR = (By.XPATH, LocatorsConstants.USER_LOGIN_XPATH)
    USER_PASSWORD_LOCATOR = (By.XPATH, LocatorsConstants.USER_PASSWORD_XPATH)
    LOGIN_BUTTON_LOCATOR = (By.XPATH, LocatorsConstants.LOGIN_BUTTON_XPATH)

    def login(self, username, password):
        self.clear_and_send_keys(self.USER_LOGIN_LOCATOR, username)
        self.clear_and_send_keys(self.USER_PASSWORD_LOCATOR, password)
        self.driver.find_element(*self.LOGIN_BUTTON_LOCATOR).click()
